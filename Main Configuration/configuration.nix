# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;


  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "America/Los_Angeles";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "en_US.UTF-8";
    LC_IDENTIFICATION = "en_US.UTF-8";
    LC_MEASUREMENT = "en_US.UTF-8";
    LC_MONETARY = "en_US.UTF-8";
    LC_NAME = "en_US.UTF-8";
    LC_NUMERIC = "en_US.UTF-8";
    LC_PAPER = "en_US.UTF-8";
    LC_TELEPHONE = "en_US.UTF-8";
    LC_TIME = "en_US.UTF-8";
  };

#Desktop Enviroment Configuration Section

 # Enable the X11 windowing system.
   services.xserver.enable = true;

 # Enable the GNOME Desktop Environment.
 # services.xserver.displayManager.gdm.enable = true;
 # services.xserver.desktopManager.gnome.enable = true;

# Enable KDE Plasma

#services.xserver.displayManager.sddm.enable = true;
#services.xserver.desktopManager.plasma5.enable = true;

# Enable Pantheon Desktop

#services.xserver.desktopManager.pantheon.enable = true;

# Enable MATE Desktop

#services.xserver.desktopManager.mate.enable = true;

# Enable Cinnamon Desktop

services.xserver.desktopManager.cinnamon.enable = true;

#Enable Deepin Desktop

#services.xserver.desktopManager.deepin.enable = true;


  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable touchpad support (enabled default in most desktopManager).
  # services.xserver.libinput.enable = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.gosh = {
    isNormalUser = true;
    description = "gosh";
    extraGroups = [ "networkmanager" "wheel" ];
    packages = with pkgs; [
    #  firefox
    #  thunderbird
    ];
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
  #  wget

#Applications to be installed!

  gnome.gnome-tweaks
  google-chrome
  vlc
  pantheon-tweaks
  distrobox
  firefox
  kitty
  kitty-themes  
  cool-retro-term
  neofetch
  appimage-run
  discord
  vscode-with-extensions
  obs-studio
  obs-studio-plugins.wlrobs
  obs-studio-plugins.obs-vaapi
  obs-studio-plugins.obs-pipewire-audio-capture
  obs-studio-plugins.advanced-scene-switcher
  obs-studio-plugins.obs-backgroundremoval
  obs-studio-plugins.obs-vintage-filter
  libreoffice-fresh
  onlyoffice-bin
  shortwave
  ripdrag
  filezilla
  calibre
  joplin-desktop
  openshot-qt
  nextcloud-client
  cider
  audacious
  dosbox
  ytmdesktop
  qbittorrent
  gpodder
  uget
  space-cadet-pinball
  brave
  openarena
  superTux
  superTuxKart
  extremetuxracer
  chromium-bsu
  hedgewars
  htop
  rclone
  rclone-browser
  appimage-run
  virt-manager
  _1password-gui
  microsoft-edge
  wget
  podman
  exa
  bat
  duf
  cmatrix
  figlet
  fortune
  python311
  python311Packages.pip
  evolutionWithPlugins
  dwt1-shell-color-scripts
  ulauncher
  virt-manager
  tldr
  mate.pluma
  gitkraken
  hplip
  emacs
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  services.flatpak.enable =true;


  # Enable the OpenSSH daemon.
   services.openssh.enable = true;

  # Open ports in the firewall.
   networking.firewall.allowedTCPPorts = [22];
   networking.firewall.allowedUDPPorts = [22];
  # Or disable the firewall altogether.
   networking.firewall.enable = true;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

programs.pantheon-tweaks.enable = true;
virtualisation.podman.enable = true; 
#virtualisation.docker.enable = true;

#Setting custom DNS resolver

  environment.etc = {
    "resolv.conf".text = "nameserver 192.168.0.35";
  };

# Enabling virtualisation using KVM

boot.kernelModules = [ "kvm-amd" "kvm-intel" ];
virtualisation.libvirtd.enable = true;

# xdg additional config for MATE and XFCE

xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
xdg.portal.enable = true;
programs.nm-applet.enable = true;

}
